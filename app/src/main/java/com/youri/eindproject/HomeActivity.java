package com.youri.eindproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.youri.eindproject.data.Bank;
import com.youri.eindproject.data.Category;
import com.youri.eindproject.data.Expense;
import com.youri.eindproject.data.InfoDatabase;
import com.youri.eindproject.accounts.AccountsFragment;
import com.youri.eindproject.overview.OverviewFragment;
import com.youri.eindproject.profile.ProfileFragment;
import com.youri.eindproject.timeline.TimelineFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

public class HomeActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    public static InfoDatabase db;
    private static final String JSON_FILE = "categories.json";
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bottomNavigationView = findViewById(R.id.bottomNav);
        bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavMethod);

        if(MainActivity.db.myDao().getCategoryCount() == 0)
            createDatabase();
        //randomExpenses();
        //createBanks();
        fragment = new OverviewFragment();

        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavMethod = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {



            switch(menuItem.getItemId()) {
                case R.id.timeline:
                    fragment = new TimelineFragment();
                    break;
                case R.id.accounts:
                    fragment = new AccountsFragment();
                    break;
                case R.id.profile:
                    fragment = new ProfileFragment();
                    break;

                    default:
                        fragment = new OverviewFragment();

            }
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

            return true;
        }
    };

    @Override
    public void onBackPressed() {}

    public void createDatabase() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("categories");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                MainActivity.db.myDao().addCategory(new Category(jo_inside.getString("name"), jo_inside.getInt("drawableId")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open(JSON_FILE);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void randomExpenses() {
        for (int i = 0; i < 200; i++) {
            int amount = (int) (Math.random() * 4) + 1;
            String name = "Buurman";
            int category = (int) (Math.random() * 40) + 1;
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, ((int) (Math.random()*26) + 1)*-1);
            boolean outgoing = true;

            Expense expense = new Expense(amount*category, category, (int)(Math.random()*2+1), name, cal, outgoing);
            MainActivity.db.myDao().addExpense(expense);
        }
    }

    public void createBanks() {
        Bank bank;
        bank = new Bank("Dikke euros");
        MainActivity.db.myDao().addBank(bank);
        bank = new Bank("Dunne euros");
        MainActivity.db.myDao().addBank(bank);
    }

    @Override
    public void onResume(){
        super.onResume();
        getSupportFragmentManager().beginTransaction().detach(fragment).attach(fragment).commit();
    }
}
