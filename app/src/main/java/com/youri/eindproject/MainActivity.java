package com.youri.eindproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.youri.eindproject.compound.NumberKeyboard;
import com.youri.eindproject.data.InfoDatabase;
import com.youri.eindproject.data.Settings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    public static InfoDatabase db;

    public static Settings settings;

    ImageView img_one, img_two, img_three, img_four;
    TextView tvGreeting;
    private int stage = 1;
    private int enteredCode = 0;
    private int correctCode = 8152;

    AnimatedVectorDrawable avd;
    Drawable img_one_draw, img_two_draw, img_three_draw, img_four_draw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = Room.databaseBuilder(getApplicationContext(), InfoDatabase.class, "infodb").allowMainThreadQueries().build();

        if(db.myDao().getSettingCount() > 0) {
            settings = db.myDao().getSetting();
        } else {
            settings = new Settings(24, 0);
            db.myDao().addSetting(settings);
        }

        correctCode = settings.getLogin();

        getComound();

        setImages();
        getMessage();
    }

    public void getComound() {
        final NumberKeyboard numberKeyboard;
        numberKeyboard = findViewById(R.id.clKeyboard);

        numberKeyboard.setListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginProcess(numberKeyboard.getNumber(v));
            }
        });
    }

    public void loading() {
        ConstraintLayout constraintLayout = findViewById(R.id.overlay);
        if(constraintLayout.getVisibility() == View.GONE)
            constraintLayout.setVisibility(View.VISIBLE);
        else
            constraintLayout.setVisibility(View.GONE);
    }

    private void checkCorrect() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                loading();
            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 1000);
        loading();

        if(enteredCode == correctCode) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        } else {
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    TextView tvError = findViewById(R.id.tvError);
                    tvError.setVisibility(View.VISIBLE);
                    resetAvd();
                }
            }, 1000);
        }
    }

    private void setImages() {
        img_one = findViewById(R.id.img1);
        img_two = findViewById(R.id.img2);
        img_three = findViewById(R.id.img3);
        img_four = findViewById(R.id.img4);

        img_one_draw = img_one.getDrawable();
        img_two_draw = img_two.getDrawable();
        img_three_draw = img_three.getDrawable();
        img_four_draw = img_four.getDrawable();
    }

    private void resetAvd() {
        avd = (AnimatedVectorDrawable) img_one_draw;
        avd.reset();
        avd = (AnimatedVectorDrawable) img_two_draw;
        avd.reset();
        avd = (AnimatedVectorDrawable) img_three_draw;
        avd.reset();
        avd = (AnimatedVectorDrawable) img_four_draw;
        avd.reset();
        avd = null;
        stage = 1;
        enteredCode = 0;
    }

    private void setUpAvd() {
        switch(stage) {
            case 1:
                avd = (AnimatedVectorDrawable) img_one_draw;
                break;
            case 2:
                avd = (AnimatedVectorDrawable) img_two_draw;
                break;
            case 3:
                avd = (AnimatedVectorDrawable) img_three_draw;
                break;
            case 4:
                avd = (AnimatedVectorDrawable) img_four_draw;
                break;

            default:
                System.out.println("Error");
        }
    }

    public void loginProcess(int input) {
        if(input >= 0) {
            setUpAvd();

            if(stage <= 4) {
                avd.start();
                enteredCode += input * Math.pow(10, (stage * -1 + 4));
                if(stage < 4)
                    stage++;
            }
            if(avd.getCurrent() == img_four_draw && avd.isRunning()) {
                checkCorrect();
            }
        } else {
            if(stage > 0) {
                avd.reset();
                enteredCode -= ((enteredCode + ((int) Math.pow(10, (stage * -1 + 4))-1))/(int) Math.pow(10,(stage * -1 + 4))) * (int) Math.pow(10,(stage * -1 + 4));
                if(stage > 1)
                    stage--;
            }
            setUpAvd();
        }

    }

    public void getMessage() {
        tvGreeting = findViewById(R.id.tv_greeting);
        DateFormat df = new SimpleDateFormat("HH");
        int hour = Integer.parseInt(df.format(Calendar.getInstance().getTime()));

        System.out.println(hour);

        if(hour >=1 && hour<=12){
            tvGreeting.setText("Good morning");
        }else if(hour>=12 && hour<=16){
            tvGreeting.setText("Good afternoon");
        }else if(hour>=16 && hour<=21){
            tvGreeting.setText("Good evening");
        }else if(hour>=21 && hour<=24){
            tvGreeting.setText("Good Night");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetAvd();
    }
}
