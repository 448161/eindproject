package com.youri.eindproject.compound;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.youri.eindproject.R;

public class NumberKeyboard extends ConstraintLayout {

    private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0;
    private ImageButton btnBack;
    private Context context;

    public NumberKeyboard(Context context) {
        super(context);
        init();
    }

    public NumberKeyboard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NumberKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        inflater.inflate(R.layout.compound_keyboard, this);
        setId();
    }

    private void setId() {
        btn0 = findViewById(R.id.btn_0);
        btn1 = findViewById(R.id.btn_1);
        btn2 = findViewById(R.id.btn_2);
        btn3 = findViewById(R.id.btn_3);
        btn4 = findViewById(R.id.btn_4);
        btn5 = findViewById(R.id.btn_5);
        btn6 = findViewById(R.id.btn_6);
        btn7 = findViewById(R.id.btn_7);
        btn8 = findViewById(R.id.btn_8);
        btn9 = findViewById(R.id.btn_9);
        btnBack = findViewById(R.id.btn_back);
    }

    public void setListeners(OnClickListener listener) {
        btn0.setOnClickListener(listener);
        btn1.setOnClickListener(listener);
        btn2.setOnClickListener(listener);
        btn3.setOnClickListener(listener);
        btn4.setOnClickListener(listener);
        btn5.setOnClickListener(listener);
        btn6.setOnClickListener(listener);
        btn7.setOnClickListener(listener);
        btn8.setOnClickListener(listener);
        btn9.setOnClickListener(listener);
        btnBack.setOnClickListener(listener);
    }

    public int getNumber(View view) {
        switch(view.getId()) {
            case R.id.btn_0:
                return 0;
            case R.id.btn_1:
                return 1;
            case R.id.btn_2:
                return 2;
            case R.id.btn_3:
                return 3;
            case R.id.btn_4:
                return 4;
            case R.id.btn_5:
                return 5;
            case R.id.btn_6:
                return 6;
            case R.id.btn_7:
                return 7;
            case R.id.btn_8:
                return 8;
            case R.id.btn_9:
                return 9;
            default:
                return -1;
        }
    }
}
