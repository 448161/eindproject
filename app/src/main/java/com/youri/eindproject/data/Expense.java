package com.youri.eindproject.data;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.youri.eindproject.items.DateTypeConverter;

import java.util.Calendar;

@Entity(tableName = "expenses")
public class Expense {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo
    private int amount;
    @ColumnInfo
    private int category;
    @ColumnInfo
    private int bank;
    @ColumnInfo
    private String name;
    @ColumnInfo
    private Calendar date;
    @ColumnInfo
    private boolean outgoing;

    public Expense(int amount, int category, int bank, String name, Calendar date, boolean outgoing) {
        this.amount = amount;
        this.category = category;
        this.bank = bank;
        this.name = name;
        this.date = date;
        this.outgoing = outgoing;
    }

    @Ignore
    public Expense() {

    }

    public int getBank() {
        return bank;
    }

    public void setBank(int bank) {
        this.bank = bank;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public boolean isOutgoing() {
        return outgoing;
    }

    public void setOutgoing(boolean outgoing) {
        this.outgoing = outgoing;
    }
}
