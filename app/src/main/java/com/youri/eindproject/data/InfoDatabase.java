package com.youri.eindproject.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.youri.eindproject.items.DateTypeConverter;

@Database(entities = {Expense.class, Category.class, Bank.class, Settings.class}, version = 1, exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class InfoDatabase extends RoomDatabase {
    public abstract MyDao myDao();
}

