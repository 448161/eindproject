package com.youri.eindproject.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Dao
public interface MyDao {

    //expense
    @Insert
    void addExpense(Expense expense);

    @Query("SELECT * from expenses")
    List<Expense> readExpense();

    @Query("SELECT * FROM expenses WHERE category = :category")
    List<Expense> readExpenseCategory(int category);

    @Query("SELECT * FROM expenses WHERE date BETWEEN :from and :to AND outgoing = 1")
    List<Expense> findExpenseBetweenDates(Calendar from, Calendar to);

    @Query("SELECT * FROM expenses WHERE date BETWEEN :from and :to AND outgoing = 1 ORDER BY date DESC")
    List<Expense> findExpenseBetweenDatesSort(Calendar from, Calendar to);

    @Query("SELECT * FROM expenses WHERE date BETWEEN :from and :to AND outgoing = 0")
    List<Expense> findIncomeBetweenDates(Calendar from, Calendar to);

    @Query("SELECT * FROM expenses WHERE bank = :bank")
    List<Expense> readExpenseBank(int bank);

    @Query("SELECT * FROM expenses WHERE outgoing = 1 ORDER BY date DESC LIMIT 3;")
    List<Expense> findLastExpenses();

    @Query("SELECT * FROM expenses WHERE category = :id AND date BETWEEN :from and :to ORDER BY date DESC")
    List<Expense> readExpenseBetweenDatesCategory(int id, Calendar from, Calendar to);

    @Query("SELECT * FROM expenses WHERE category = :id AND date BETWEEN :from and :to")
    List<Expense> findExpenseBetweenDatesCategory(int id, Calendar from, Calendar to);

    //categories
    @Insert
    void addCategory(Category category);

    @Query("SELECT * from categories")
    List<Category> readCategories();

    @Query("SELECT * FROM categories WHERE id = :id LIMIT 1;")
    Category category(int id);

    @Query("SELECT * from expenses WHERE category = :category")
    List<Expense> getCategoryExpenses(int category);

    @Query("SELECT COUNT(id) FROM categories")
    int getCategoryCount();

    @Query("SELECT name FROM categories WHERE id = :id LIMIT 1;")
    String getCategoryNameById(int id);

    @Query("SELECT drawableId FROM categories WHERE id = :id LIMIT 1;")
    int getDrawableIdById(int id);

    //bank
    @Insert
    void addBank(Bank bank);

    @Query("SELECT * FROM banks")
    List<Bank> readBank();

    //settings
    @Insert
    void addSetting(Settings settings);

    @Query("SELECT * FROM settings LIMIT 1;")
    Settings getSetting();

    @Query("SELECT COUNT(id) FROM settings")
    int getSettingCount();

    @Update
    void updateSettings(Settings settings);
}
