package com.youri.eindproject.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.youri.eindproject.R;
import com.youri.eindproject.items.SelectDayAdapter;

import java.util.ArrayList;

public class SelectDayDialog {

    private Activity activity;
    private AlertDialog alertDialog;

    public SelectDayDialog(Activity activity) {
        this.activity = activity;
    }

    public void startDialog(AdapterView.OnItemClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_select_day,null);

        ListView listView = view.findViewById(R.id.list);

        listView.setAdapter(new SelectDayAdapter(view.getContext(), getStrings()));
        //listView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        listView.setOnItemClickListener(listener);

        builder.setView(view);
        builder.setCancelable(true);

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void dismissDialog() {
        alertDialog.dismiss();
    }

    public ArrayList<String> getStrings() {
        ArrayList<String> strings = new ArrayList<>();

        for (int i = 1; i <= 28; i++) {
            if(i == 1 || i == 21)
                strings.add(i+"st day of every month");
            else if(i == 2 || i == 22)
                strings.add(i+"nd day of every month");
            else if(i == 3 || i == 23)
                strings.add(i+"rd day of every month");
            else
                strings.add(i+"th day of every month");
        }

        return strings;
    }
}
