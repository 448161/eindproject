package com.youri.eindproject.items;

import androidx.room.TypeConverter;

import java.util.Calendar;

public class DateTypeConverter {

    @TypeConverter
    public static Calendar fromTimestamp(Long value) {
        Calendar cal = Calendar.getInstance();
        if(value!=null)
            cal.setTimeInMillis(value);
        return value == null ? null : cal;
    }

    @TypeConverter
    public static Long dateToTimestamp(Calendar cal) {
        return cal == null ? null : cal.getTimeInMillis();
    }

}
