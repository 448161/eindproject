package com.youri.eindproject.items;
import com.youri.eindproject.R;

import java.util.ArrayList;

public class DrawableIdIconProvider {
    private static ArrayList<Integer> icons;

    static {
        icons = new ArrayList<>();

        icons.add(R.drawable.cat_ic_home);
        icons.add(R.drawable.cat_ic_housegarden);
        icons.add(R.drawable.cat_ic_fooddrinks);
        icons.add(R.drawable.cat_ic_transport);
        icons.add(R.drawable.cat_ic_shopping);
        icons.add(R.drawable.cat_ic_leisure);
        icons.add(R.drawable.cat_ic_healthbeauty);
        icons.add(R.drawable.cat_ic_other);
    }

    public static int getId(int id) {
        return icons.get(id-1) == null ? 0 : icons.get(id-1);
    }
}
