package com.youri.eindproject.items;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;


import java.util.ArrayList;


public class SelectDayAdapter extends ArrayAdapter<String> {

    private ArrayList<String> strings;
    private Context context;

    public SelectDayAdapter(@NonNull Context context, ArrayList<String> strings) {
        super(context, 0, strings);
        this.strings = strings;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.item_dialog_select_day, parent, false);

        TextView date = convertView.findViewById(R.id.day);
        ImageView checkMark = convertView.findViewById(R.id.checkMark);

        if(MainActivity.settings.getSalaryDay() == position+1){
            checkMark.setVisibility(View.VISIBLE);
            date.setTextColor(Color.GRAY);
        } else {
            checkMark.setVisibility(View.INVISIBLE);
            date.setTextColor(Color.BLACK);
        }

        date.setText(strings.get(position));

        return convertView;
    }
}
