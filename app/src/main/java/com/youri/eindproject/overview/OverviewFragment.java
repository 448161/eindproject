package com.youri.eindproject.overview;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.overview.activities.TransactionsActivity;
import com.youri.eindproject.data.Category;
import com.youri.eindproject.data.Expense;
import com.youri.eindproject.dialogs.SelectDayDialog;
import com.youri.eindproject.overview.items.BanksAdapter;
import com.youri.eindproject.overview.items.OverviewAdapter;
import com.youri.eindproject.overview.items.TransactionsAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OverviewFragment extends Fragment {

    private RecyclerView recyclerView, banks, transactions;
    private TextView tvDate, tvOutcome, tvIncome, tvRemaining;
    private int highest;
    private Button btnView, btnDay;
    private Fragment fragment;

    private List<Expense> expenses, income;

    public OverviewFragment() {
        fragment = this;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_overview, container, false);

        view.findViewById(R.id.tv_title).requestFocus();

        tvDate = view.findViewById(R.id.tvDate);
        tvOutcome = view.findViewById(R.id.tvOutcomeAmount);
        tvIncome = view.findViewById(R.id.tvIncomeAmount);
        tvRemaining = view.findViewById(R.id.tvRemainingAmount);

        recyclerView = view.findViewById(R.id.overviewList);
        banks = view.findViewById(R.id.horizontalRecycler);
        transactions = view.findViewById(R.id.reyclerbanks);

        btnView = view.findViewById(R.id.btnView);
        btnDay = view.findViewById(R.id.button);

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked!");
                Intent intent = new Intent(getActivity(), TransactionsActivity.class);
                startActivity(intent);
            }
        });

        btnDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SelectDayDialog dialog = new SelectDayDialog(getActivity());
                dialog.startDialog(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MainActivity.settings.setSalaryDay(position+1);
                        MainActivity.db.myDao().updateSettings(MainActivity.settings);
                        dialog.dismissDialog();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(fragment).attach(fragment).commit();
                    }
                });
            }
        });

        setTvDate();
        highest = 0;
        expenses = getData(true);
        income = getData(false);

        setText(expenses, income);

        adapters();

        return view;
    }

    private void adapters() {
        OverviewAdapter adapter = new OverviewAdapter(sortCategories(expenses), highest, this.getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        BanksAdapter banksAdapter = new BanksAdapter(MainActivity.db.myDao().readBank());
        banks.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.HORIZONTAL, false));
        banks.setAdapter(banksAdapter);

        TransactionsAdapter transactionsAdapter = new TransactionsAdapter(MainActivity.db.myDao().findLastExpenses(), this.getContext());
        transactions.setLayoutManager(new LinearLayoutManager(this.getContext()));
        transactions.setAdapter(transactionsAdapter);
    }

    private void setText(List<Expense> expenses, List<Expense> income) {
        int totalIncome = 0, totalExpense = 0;

        for(Expense expense : expenses) {
            totalExpense += expense.getAmount();
        }
        tvOutcome.setText("€"+totalExpense);
        for(Expense inc : income) {
            totalIncome += inc.getAmount();
        }
        tvIncome.setText("€"+totalIncome);
        tvRemaining.setText("€"+(totalIncome-totalExpense));
    }

    private List<Expense> getData(boolean outgoing) {
        Calendar calendar = Calendar.getInstance();
        if(calendar.get(Calendar.DAY_OF_MONTH) < MainActivity.settings.getSalaryDay())
            calendar.add(Calendar.MONTH, -1);

        calendar.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());

        if(outgoing)
            return MainActivity.db.myDao().findExpenseBetweenDates(calendar, Calendar.getInstance());
        else
            return MainActivity.db.myDao().findIncomeBetweenDates(calendar, Calendar.getInstance());
    }

    private void setTvDate() {
        DateFormat df = new SimpleDateFormat("d MMM");
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();

        if(calendar.get(Calendar.DAY_OF_MONTH) < MainActivity.settings.getSalaryDay())
            calendar.add(Calendar.MONTH, -1);
        else
            calendar1.add(Calendar.MONTH, 1);

        calendar.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());

        calendar1.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());
        calendar1.add(Calendar.DAY_OF_MONTH, -1);

        tvDate.setText(df.format(calendar.getTime()) + " - " + df.format(calendar1.getTime()));
    }

    private List<Category> sortCategories(List<Expense> expenseList) {
        List<Category> categoryList = MainActivity.db.myDao().readCategories();
        List<Category> categories = new ArrayList<>();
        List<Category> cutCategories = new ArrayList<>();
        for(Expense expense : expenseList){
            if(!categories.contains(categoryList.get(expense.getCategory()-1)))
                categories.add(categoryList.get(expense.getCategory()-1));
        }

        for (int i = 0; i < categories.size(); i++) {
            Category category = categories.get(i);

            Calendar calendar = Calendar.getInstance();
            if(calendar.get(Calendar.DAY_OF_MONTH) < MainActivity.settings.getSalaryDay())
                calendar.add(Calendar.MONTH, -1);

            calendar.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());
            List<Expense> expenses = MainActivity.db.myDao().findExpenseBetweenDatesCategory(category.getId(), calendar, Calendar.getInstance());
            int amount = 0;

            for(Expense expense : expenses) {
                amount += expense.getAmount();
            }
            category.setAmount(amount);
            if(amount>highest)
                highest = amount;
        }

        Collections.sort(categories, new Comparator<Category>(){
            public int compare(Category o1, Category o2) {
                return o1.getAmount() - o2.getAmount();
            }
        });

        int n = 0;

        for (int i = categories.size()-1; i >= 0; i--) {
            if(n > 6)
                break;
            Category category = categories.get(i);
            Calendar calendar = Calendar.getInstance();
            if(calendar.get(Calendar.DAY_OF_MONTH) < MainActivity.settings.getSalaryDay())
                calendar.add(Calendar.MONTH, -1);

            calendar.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());
            List<Expense> expenses = MainActivity.db.myDao().findExpenseBetweenDatesCategory(category.getId(), calendar, Calendar.getInstance());
            int amount = 0;

            for(Expense expense : expenses) {
                amount += expense.getAmount();
            }

            if(100*amount/highest > 5) {
                cutCategories.add(category);
                n++;
            }
        }

        return cutCategories;
    }
}

