package com.youri.eindproject.overview.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.data.Category;
import com.youri.eindproject.data.Expense;
import com.youri.eindproject.overview.items.ExpenseAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    private TextView categoryName, date, tvReceivedAmount, tvSpentAmount, tvTotalAmount;
    private RecyclerView recyclerView;
    private int income, outcome;
    private List<Expense> expenseList;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Category category = MainActivity.db.myDao().category(getIntent().getIntExtra("CATEGORY", 0));

        categoryName = findViewById(R.id.category);
        date = findViewById(R.id.date);

        tvReceivedAmount = findViewById(R.id.tvReceivedAmount);
        tvSpentAmount = findViewById(R.id.tvSpentAmount);
        tvTotalAmount = findViewById(R.id.tvTotalAmount);

        btnBack = findViewById(R.id.btn_back);

        categoryName.setText(category.getName());

        DateFormat df = new SimpleDateFormat("EEEE d MMMM");

        Calendar calender = Calendar.getInstance();
        if(calender.get(Calendar.DAY_OF_MONTH) < MainActivity.settings.getSalaryDay())
            calender.add(Calendar.MONTH, -2);
        calender.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());
        calender.add(Calendar.MONTH, -1);

        date.setText("Totals since " + df.format(calender.getTime()));

        expenseList = MainActivity.db.myDao().readExpenseBetweenDatesCategory(category.getId(),calender, Calendar.getInstance());
        for(Expense expense : expenseList) {
            if(expense.isOutgoing())
                outcome += expense.getAmount();
            else
                income += expense.getAmount();
        }

        tvReceivedAmount.setText("€"+income);
        tvSpentAmount.setText("-€"+outcome);
        if(outcome>income)
            tvTotalAmount.setText("-€"+(outcome+income));
        else
            tvTotalAmount.setText("€"+(outcome+income));

        recyclerView = findViewById(R.id.transactionList);
        ExpenseAdapter adapter = new ExpenseAdapter(this, expenseList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
