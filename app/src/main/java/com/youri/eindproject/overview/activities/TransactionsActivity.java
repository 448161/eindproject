package com.youri.eindproject.overview.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.data.Expense;
import com.youri.eindproject.overview.items.ExpenseAdapter;

import java.util.Calendar;
import java.util.List;

public class TransactionsActivity extends AppCompatActivity {

    private Button btnBack;
    private RecyclerView recyclerView;
    private List<Expense> expenseList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        btnBack = findViewById(R.id.btn_back);

        Calendar calendar = Calendar.getInstance();
        if(calendar.get(Calendar.DAY_OF_MONTH) < MainActivity.settings.getSalaryDay())
            calendar.add(Calendar.MONTH, -1);

        calendar.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());

        expenseList = MainActivity.db.myDao().findExpenseBetweenDatesSort(calendar, Calendar.getInstance());

        recyclerView = findViewById(R.id.transactionList);
        ExpenseAdapter adapter = new ExpenseAdapter(this, expenseList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
