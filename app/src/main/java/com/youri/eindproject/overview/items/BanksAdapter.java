package com.youri.eindproject.overview.items;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.data.Bank;
import com.youri.eindproject.data.Expense;

import java.util.List;

public class BanksAdapter extends RecyclerView.Adapter<BanksAdapter.ViewHolder> {

    private List<Bank> banks;

    public BanksAdapter(List<Bank> banks) {
        this.banks = banks;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_overview_bank, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Bank bank = banks.get(position);

        holder.name.setText(bank.getName());
        List<Expense> expenseList = MainActivity.db.myDao().readExpenseBank(bank.getId());
        int amount = 0;
        for(Expense expense : expenseList) {
            if(expense.isOutgoing())
                amount -= expense.getAmount();
            else
                amount += expense.getAmount();
        }
        holder.amount.setText("€"+amount);
    }

    @Override
    public int getItemCount() {
        return banks == null ? 0 : banks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ConstraintLayout bankcard;
        TextView amount, name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            bankcard = itemView.findViewById(R.id.bankcard);

            amount = itemView.findViewById(R.id.amount);
            name = itemView.findViewById(R.id.name);
        }
    }


}
