package com.youri.eindproject.overview.items;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class CustomView extends View {

    private RectF rect;
    private Paint paint;


    public CustomView(Context context) {
        super(context);

        init(null);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }

    private void init(@Nullable AttributeSet set) {
        rect = new RectF();
        paint = new Paint();
    }

    public void setBar(int opacity) {
        rect.right = opacity;

        String color  = "#" + String.format("%x", opacity) + "00ab6e";

        //System.out.println("Color: " + color + " with an opacity of: " + opacity);

        paint.setColor(Color.parseColor(color));

        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        rect.right = rect.right * getWidth() / 255;
        rect.bottom = getHeight();

        canvas.drawRoundRect(rect,15,15, paint);

    }
}
