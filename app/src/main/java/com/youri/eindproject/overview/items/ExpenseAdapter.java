package com.youri.eindproject.overview.items;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.data.Expense;
import com.youri.eindproject.items.DrawableIdIconProvider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ViewHolder> {

    private List<Expense> expenseList;
    private Context context;
    private Calendar lastTime;

    public ExpenseAdapter(Context context, List<Expense> expenseList) {
        this.expenseList = expenseList;
        this.context = context;
        this.lastTime = Calendar.getInstance();
        this.lastTime.setTimeInMillis(0);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expense, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Expense expense = expenseList.get(position);
        if(lastTime.get(Calendar.DAY_OF_MONTH) != expense.getDate().get(Calendar.DAY_OF_MONTH)) {
            holder.tvDate.setText(getDate(expense.getDate()));
            holder.tvDate.setVisibility(View.VISIBLE);
        } else {
            holder.tvDate.setVisibility(View.GONE);
        }
        lastTime.setTimeInMillis(expense.getDate().getTimeInMillis());

        holder.tvTransaction.setText(expense.getName());
        if(expense.isOutgoing())
            holder.tvTransactionAmount.setText("-€"+expense.getAmount());
        else
            holder.tvTransactionAmount.setText("€"+expense.getAmount());
        holder.tvCategory.setText(MainActivity.db.myDao().getCategoryNameById(expense.getCategory()));

        holder.categoryDrawable.setImageDrawable(ContextCompat.getDrawable(context , DrawableIdIconProvider.getId(MainActivity.db.myDao().getDrawableIdById(expense.getCategory()))));
        holder.categoryDrawable.setColorFilter(Color.parseColor("#00ab6e"));
    }

    @Override
    public int getItemCount() {
        return expenseList == null ? 0 : expenseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDate, tvTransaction, tvCategory, tvTransactionAmount;
        private ImageView categoryDrawable;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tvDate);
            tvTransaction = itemView.findViewById(R.id.tvTransaction);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvTransactionAmount = itemView.findViewById(R.id.tvTransactionAmount);

            categoryDrawable = itemView.findViewById(R.id.categoryDrawable);
        }
    }

    private String getDate(Calendar transactionDate) {
        String date = "";

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        if(transactionDate.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH))
            date = "Today";
        calendar.add(Calendar.DATE, -1);

        if(transactionDate.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH))
            date = "Yesterday";
        calendar.add(Calendar.DATE, -1);

        if(transactionDate.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH))
            date = "Day before yesterday";

        if(date.equals("")){
            DateFormat df;

            df = new SimpleDateFormat("yyyy");
            if(df.format(calendar.getTime()).equals(df.format(transactionDate.getTime())))
                df = new SimpleDateFormat("EEEE d MMMM");
            else
                df = new SimpleDateFormat("EEEE d MMMM yyyy");

            date = df.format(transactionDate.getTime());
        }

        return date;
    }
}
