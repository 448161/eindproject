package com.youri.eindproject.overview.items;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.youri.eindproject.overview.activities.CategoryActivity;
import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.data.Category;
import com.youri.eindproject.data.Expense;
import com.youri.eindproject.items.DrawableIdIconProvider;

import java.util.Calendar;
import java.util.List;

public class OverviewAdapter extends RecyclerView.Adapter<OverviewAdapter.ViewHolder> {

    private static final String TAG = "OverviewAdapter";
    private List<Category> categoryList;
    private int max_amount;
    private Context context;

    public OverviewAdapter(List<Category> categoryList, int max_amount, Context context) {
        this.categoryList = categoryList;
        this.max_amount = max_amount;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_overview_expense, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Category category = categoryList.get(position);

        Calendar calendar = Calendar.getInstance();
        if(calendar.get(Calendar.DAY_OF_MONTH) < MainActivity.settings.getSalaryDay())
            calendar.add(Calendar.MONTH, -1);

        calendar.set(Calendar.DAY_OF_MONTH, MainActivity.settings.getSalaryDay());

        List<Expense> expenses = MainActivity.db.myDao().findExpenseBetweenDatesCategory(category.getId(), calendar, Calendar.getInstance());

        int amount = 0;

        for(Expense expense : expenses) {
            amount += expense.getAmount();
        }

        holder.tvCategory.setText(category.getName());
        holder.tvCategoryAmount.setText("€"+amount);
        if(categoryList.size() == position+1)
            holder.view.setVisibility(View.GONE);

        holder.customView.setBar((255*amount)/max_amount);

        holder.imageView.setImageDrawable(ContextCompat.getDrawable(context , DrawableIdIconProvider.getId(category.getDrawableId())));
        holder.imageView.setColorFilter(Color.parseColor("#00ab6e"));

        holder.parentConstraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CategoryActivity.class);
                intent.putExtra("CATEGORY", category.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList == null ? 0 : categoryList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        View view;

        CustomView customView;

        ConstraintLayout parentConstraint;

        ImageView imageView;
        TextView tvCategory, tvCategoryAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.line);

            imageView = itemView.findViewById(R.id.categoryDrawable);

            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvCategoryAmount = itemView.findViewById(R.id.tvCategoryAmount);

            customView = itemView.findViewById(R.id.cvBar);

            parentConstraint = itemView.findViewById(R.id.parent_constraint);
        }
    }
}
