package com.youri.eindproject.overview.items;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.data.Category;
import com.youri.eindproject.data.Expense;
import com.youri.eindproject.items.DrawableIdIconProvider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    List<Expense> expenseList;
    Context context;

    public TransactionsAdapter(List<Expense> expenseList, Context context) {
        this.context = context;
        this.expenseList = expenseList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_overview_transaction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Expense expense = expenseList.get(position);

        holder.tvTransaction.setText(expense.getName());
        holder.tvTransactionAmount.setText("-€"+expense.getAmount());

        holder.tvDate.setText(getDate(expense.getDate()));

        Category category = MainActivity.db.myDao().category(expense.getCategory());

        holder.categoryDrawable.setImageDrawable(ContextCompat.getDrawable(context , DrawableIdIconProvider.getId(category.getDrawableId())));
        holder.categoryDrawable.setColorFilter(Color.parseColor("#00ab6e"));
    }

    @Override
    public int getItemCount() {
        return expenseList == null ? 0 : expenseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView categoryDrawable;

        TextView tvTransaction, tvTransactionAmount, tvDate;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            categoryDrawable = itemView.findViewById(R.id.categoryDrawable);

            tvTransaction = itemView.findViewById(R.id.tvTransaction);
            tvTransactionAmount = itemView.findViewById(R.id.tvTransactionAmount);
            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }

    private String getDate(Calendar transactionDate) {
        String date = "";

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        if(transactionDate.getTimeInMillis()>calendar.getTimeInMillis())
            date = "Today";
        calendar.add(Calendar.DATE, -1);

        if(transactionDate.getTimeInMillis()>calendar.getTimeInMillis())
            date = "Yesterday";
        calendar.add(Calendar.DATE, -1);

        if(transactionDate.getTimeInMillis()>calendar.getTimeInMillis())
            date = "Day before yesterday";

        if(date.equals("")){
            DateFormat df;

            df = new SimpleDateFormat("yyyy");
            if(df.format(calendar.getTime()).equals(df.format(transactionDate.getTime())))
                df = new SimpleDateFormat("EEEE d MMMM");
            else
                df = new SimpleDateFormat("EEEE d MMMM yyyy");

            date = df.format(transactionDate.getTime());
        }

        return date;
    }
}
