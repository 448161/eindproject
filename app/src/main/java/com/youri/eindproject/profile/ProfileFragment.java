package com.youri.eindproject.profile;


import android.content.Intent;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.dialogs.SelectDayDialog;
import com.youri.eindproject.profile.items.BankAdapter;
import com.youri.eindproject.items.CustomListView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private TextView tvDate, tvChangepin, tvCategory, tvAbout, tvLogout;
    private ConstraintLayout salary, add;

    private Fragment fragment = this;

    private CustomListView listView;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        setViewId(view);
        setListView();
        setListeners();

        setText();

        return view;
    }

    private void setViewId(View view) {
        listView = view.findViewById(R.id.lvBanks);

        add = view.findViewById(R.id.add);
        salary = view.findViewById(R.id.salary);

        tvDate = view.findViewById(R.id.tvDate);
        tvChangepin = view.findViewById(R.id.tvChangepin);
        tvCategory = view.findViewById(R.id.tvCategory);
        tvAbout = view.findViewById(R.id.tvAbout);
        tvLogout = view.findViewById(R.id.tvLogout);
    }

    private void setListeners() {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        salary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SelectDayDialog dialog = new SelectDayDialog(getActivity());
                dialog.startDialog(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MainActivity.settings.setSalaryDay(position+1);
                        MainActivity.db.myDao().updateSettings(MainActivity.settings);
                        dialog.dismissDialog();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(fragment).attach(fragment).commit();
                    }
                });
            }
        });


        tvChangepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private void setListView() {
        BankAdapter bankAdapter = new BankAdapter(getContext(), MainActivity.db.myDao().readBank());
        listView.setAdapter(bankAdapter);
    }

    private void setText() {
        tvDate.setText(MainActivity.settings.getSalaryDay()+addition());
    }

    private String addition() {
        String s;
        if(MainActivity.settings.getSalaryDay() == 1 || MainActivity.settings.getSalaryDay() == 21)
            s = "st";
        else if(MainActivity.settings.getSalaryDay() == 2 || MainActivity.settings.getSalaryDay() == 22)
            s = "nd";
        else if(MainActivity.settings.getSalaryDay() == 3 || MainActivity.settings.getSalaryDay() == 23)
            s = "rd";
        else
            s = "th";
        return s;
    }
}
