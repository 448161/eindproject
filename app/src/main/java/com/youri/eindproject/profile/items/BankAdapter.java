package com.youri.eindproject.profile.items;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.youri.eindproject.MainActivity;
import com.youri.eindproject.R;
import com.youri.eindproject.data.Bank;
import com.youri.eindproject.data.Expense;

import java.util.List;

public class BankAdapter extends ArrayAdapter<Bank> {

    private List<Bank> bankList;
    private Context context;

    public BankAdapter(@NonNull Context context, @NonNull List<Bank> bankList) {
        super(context, 0, bankList);
        this.bankList = bankList;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.item_profile_bank, parent, false);

        int amount = 0;
        Bank bank = bankList.get(position);

        List<Expense> expenseList = MainActivity.db.myDao().readExpenseBank(bank.getId());

        for(Expense expense : expenseList) {
            if(expense.isOutgoing())
                amount -= expense.getAmount();
            else
                amount += expense.getAmount();
        }

        TextView tvBank = convertView.findViewById(R.id.tvBank);
        TextView tvAmount = convertView.findViewById(R.id.tvAmount);

        ImageView icon = convertView.findViewById(R.id.imgIcon);

        tvBank.setText(bank.getName());
        if(amount <0) {
            tvAmount.setText("-€" + amount);
            tvAmount.setTextColor(Color.parseColor("#c44743"));
            //icon.setBackgroundColor(Color.parseColor("#c44743"));
        } else {
            tvAmount.setText("€" + amount);
            tvAmount.setTextColor(Color.parseColor("#00aa6c"));
            //icon.setBackgroundColor(Color.parseColor("#00aa6c"));
        }



        return convertView;
    }
}
